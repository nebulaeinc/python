import table
import unittest
from unittest import TestCase



class CSVParserTest(TestCase):
    """ Main test class for table.py """

    sample_csv = [
        {"sizes":"44|55|33", "internal_id":"1", "color":"red"},
        {"sizes":"xl|xs|m", "internal_id":"2", "color":"blue"},
        {"sizes":"55|33", "internal_id":"3", "color":"red"},
        {"sizes":"22|33", "internal_id":"4", "color":"orange"},
        {"sizes":"", "internal_id":"5", "color":"red"},
        {"sizes":"44|xl|33|m|s", "internal_id":"6", "color":"yellow"}
    ]

    sample_csv_err = [
        {"sizes":"44|55|33", "internal_id":"1", "color":"red"},
        {"szes":"xl|xs|m", "internal_id":"2", "color":"blue"},
        {"sizes":"55|33", "internal_id":"3", "color":"red"},
        {"sizes":"22|33", "internal_id":"4", "color":"orange"},
        {"sizes":"", "internal_id":"5", "color":"red"},
        {"sizes":"44|xl|33|m|s", "internal_id":"6", "color":"yellow"}
    ]

    sizes = ["44","55", "33","xl","xs","m","22","","s"]

    def setUp(self):
        self.parser = table.CSVParser()
        self.parser.parsedFile = CSVParserTest.sample_csv

    def test_sizes_parsed(self):
        self.parser.size_parser()
        self.assertEqual(CSVParserTest.sizes, self.parser.sizes)

        self.parser.parsedFile = CSVParserTest.sample_csv_err
        self.assertRaises(KeyError, self.parser.size_parser)

    def test_group_file_created(self):
        self.parser.sizes = CSVParserTest.sizes
        self.parser.create_grouped_file()

        self.assertEqual( "44" , self.parser.groupedFile[0]["size"])
        self.assertEqual( "55" , self.parser.groupedFile[1]["size"])
        self.assertEqual( "xl" , self.parser.groupedFile[3]["size"])
        #sanity check
        self.assertNotEqual( "sd" , self.parser.groupedFile[5]["internal_ids"])
        self.assertEqual( "" , self.parser.groupedFile[5]["colours"])


    def test_group_item(self):
        self.parser.sizes = CSVParserTest.sizes
        self.parser.create_grouped_file()
        item = {"size":"22", "internal_id":"12", "color":"red"}
        item2 = {"size":"22", "internal_id":"13", "color":"red"}
        item3 = {"size":"22", "internal_id":"14", "color":"orange"}
        self.assertEqual("12", self.parser.group_item( 0 , item))

        self.assertEqual("12|13", self.parser.group_item( 0 , item2))

        self.assertEqual("12|13|14", self.parser.group_item( 0 , item3))


    def test_assign_colour(self):
        self.parser.sizes = CSVParserTest.sizes
        self.parser.create_grouped_file()
        item = {"size":"22", "internal_id":"12", "color":"red"}
        item2 = {"size":"22", "internal_id":"13", "color":"red"}
        item3 = {"size":"22", "internal_id":"14", "color":"orange"}
        self.assertEqual("red", self.parser.assign_colour( 0 , item))

        self.assertEqual("red", self.parser.assign_colour( 0 , item2))

        self.assertEqual("red|orange", self.parser.assign_colour( 0 , item3))

if __name__ == "__main__":
    unittest.main()
