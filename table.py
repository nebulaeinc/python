import csv
import re
#import pdb; pdb.set_trace()

class CSVParser:
    """ """

    def __init__(self, csvFile = None, outputFile = None):
        """ initialise variables here  -
            opted to go with more memory
            usage over more recursion """
        self.fileName = csvFile # the name of the file to parse
        self.outputFile = outputFile # the output destination
        self.parsedFile = [] # list of dict objects
        self.groupedFile = [] # list of dict objects each of which is a group representing a size_parse
        self.sizes = [] #list of sizes || helps cut recursion and quicker finding of values
        pass


    def reader(self):
        """ read in the current version of csv file """
        with open(self.fileName, newline='') as csvFile:
            reader = csv.DictReader(csvFile)
            for row in reader:
                self.parsedFile.append(row)
                #self.parsedFile.append({"sizes":row['sizes'], "price":row['price']})

    def writer(self):
        """write the final version modified version"""
        keys = ['size','internal_ids','colours']
        with open(self.outputFile, 'w', newline='', encoding='utf8') as output:
            dict_writer = csv.DictWriter(output, keys)
            dict_writer.writeheader()
            for row in self.groupedFile:
                dict_writer.writerow(row)
        return True


    def size_parser(self):
        """get a list of all the sizes of the file"""
        try:
            for row in self.parsedFile:
                sizes = re.compile("[|]").split(row["sizes"])
                for size in sizes:
                    if size not in self.sizes:
                        self.sizes.append(size)
            return self.sizes #for testing purposes
        except KeyError as e:
            raise


    def create_grouped_file(self):
        """ """
        for size in self.sizes: # create the output file structure
            self.groupedFile.append({ "size":size, "internal_ids":'', "colours":''})
        return self.groupedFile



    def group(self):
        """ iterate row by row and through parsedFile and add the
         element to the corresponding size dict in groupedFile """
        try:
            for row in self.parsedFile:
                for size in re.compile("[|]").split(row["sizes"]):
                    index = self.sizes.index(size) #assumes self.size and self.groupedFile are mapped correctly
                    self.group_item(index, row)
                    self.assign_colour(index, row)
            return self.groupedFile #for testing purposes
        except Exception as e:
            return e


    def group_item(self, index, item):
        """the main block that groups items"""
        ids = self.groupedFile[index]["internal_ids"]
        if( ids == "" ):
            self.groupedFile[index]["internal_ids"] =  item["internal_id"]
        else:
            self.groupedFile[index]["internal_ids"] = ids + "|" + item["internal_id"]
        return self.groupedFile[index]["internal_ids"]  #for testing purposes



    def assign_colour(self, index, item):
        """assigns the colour of the item to the group if not already exists"""
        colours = self.groupedFile[index]["colours"]
        if( colours == ''):
            self.groupedFile[index]["colours"] = item["color"]
            print(index.__str__() + " " + item['color'])
        else:
            if item["color"] not in colours:
                self.groupedFile[index]["colours"] = colours + "|" + item["color"]
        return self.groupedFile[index]["colours"] #for testing purposes



def main():
    parser = CSVParser('test_feed.csv', 'out_feed.csv')
    parser.reader()
    parser.size_parser()
    parser.create_grouped_file()
    parser.group()
    parser.writer()


if  __name__ == '__main__':
    main()
